EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x07_Odd_Even J?
U 1 1 6E5D11A1
P 4950 3250
F 0 "J?" H 5000 3767 50  0000 C CNN
F 1 "Conn_02x07_Odd_Even" H 5000 3676 50  0000 C CNN
F 2 "" H 4950 3250 50  0001 C CNN
F 3 "~" H 4950 3250 50  0001 C CNN
	1    4950 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 3150 4750 3050
Connection ~ 4750 2950
Wire Wire Line
	4750 2950 4750 2550
Connection ~ 4750 3050
Wire Wire Line
	4750 3050 4750 2950
Wire Wire Line
	5250 3150 5250 3050
Wire Wire Line
	5250 2550 4750 2550
Connection ~ 5250 2950
Wire Wire Line
	5250 2950 5250 2550
Connection ~ 5250 3050
Wire Wire Line
	5250 3050 5250 2950
Wire Wire Line
	5250 3350 5250 3450
Connection ~ 5250 3450
Wire Wire Line
	5250 3450 5250 3550
Connection ~ 5250 3550
Wire Wire Line
	5250 3550 5250 3900
Wire Wire Line
	5250 3900 4750 3900
Wire Wire Line
	4750 3900 4750 3550
Connection ~ 4750 3450
Wire Wire Line
	4750 3450 4750 3350
Connection ~ 4750 3550
Wire Wire Line
	4750 3550 4750 3450
Wire Wire Line
	4750 3250 4200 3250
Wire Wire Line
	3650 3250 3650 2350
Wire Wire Line
	3650 2350 4200 2350
Wire Wire Line
	4200 2350 4200 3250
Connection ~ 4200 3250
Wire Wire Line
	4200 3250 3650 3250
Wire Wire Line
	5250 3250 5700 3250
Wire Wire Line
	6250 3250 6250 2350
Wire Wire Line
	6250 2350 5700 2350
Wire Wire Line
	5700 2350 5700 3250
Connection ~ 5700 3250
Wire Wire Line
	5700 3250 6250 3250
$EndSCHEMATC
