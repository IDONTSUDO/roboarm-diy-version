EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x07_Odd_Even J?
U 1 1 6E5D370E
P 5450 3150
F 0 "J?" H 5500 3667 50  0000 C CNN
F 1 "Conn_02x07_Odd_Even" H 5500 3576 50  0000 C CNN
F 2 "" H 5450 3150 50  0001 C CNN
F 3 "~" H 5450 3150 50  0001 C CNN
	1    5450 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 3050 5750 2950
Connection ~ 5750 2850
Wire Wire Line
	5750 2850 5750 2450
Connection ~ 5750 2950
Wire Wire Line
	5750 2950 5750 2850
Wire Wire Line
	5250 3050 5250 2950
Connection ~ 5250 2850
Connection ~ 5250 2950
Wire Wire Line
	5250 2950 5250 2850
Wire Wire Line
	5250 3250 5250 3350
Connection ~ 5250 3350
Wire Wire Line
	5250 3350 5250 3450
Connection ~ 5250 3450
Wire Wire Line
	5250 3450 5250 3750
Wire Wire Line
	5750 3250 5750 3350
Connection ~ 5750 3350
Wire Wire Line
	5750 3350 5750 3450
Connection ~ 5750 3450
Wire Wire Line
	5250 3150 4850 3150
Wire Wire Line
	5750 3150 6100 3150
$Comp
L Mechanical:MountingHole_Pad H?
U 1 1 6E5D5709
P 6100 3050
F 0 "H?" H 6200 3099 50  0000 L CNN
F 1 "MountingHole_Pad" H 6200 3008 50  0000 L CNN
F 2 "" H 6100 3050 50  0001 C CNN
F 3 "~" H 6100 3050 50  0001 C CNN
	1    6100 3050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H?
U 1 1 6E5D5C53
P 6500 3050
F 0 "H?" H 6600 3099 50  0000 L CNN
F 1 "MountingHole_Pad" H 6600 3008 50  0000 L CNN
F 2 "" H 6500 3050 50  0001 C CNN
F 3 "~" H 6500 3050 50  0001 C CNN
	1    6500 3050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H?
U 1 1 6E5D5F30
P 6850 3050
F 0 "H?" H 6950 3099 50  0000 L CNN
F 1 "MountingHole_Pad" H 6950 3008 50  0000 L CNN
F 2 "" H 6850 3050 50  0001 C CNN
F 3 "~" H 6850 3050 50  0001 C CNN
	1    6850 3050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H?
U 1 1 6E5D62C6
P 4850 3050
F 0 "H?" H 4950 3099 50  0000 L CNN
F 1 "MountingHole_Pad" H 4950 3008 50  0000 L CNN
F 2 "" H 4850 3050 50  0001 C CNN
F 3 "~" H 4850 3050 50  0001 C CNN
	1    4850 3050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H?
U 1 1 6E5D679D
P 4500 3050
F 0 "H?" H 4600 3099 50  0000 L CNN
F 1 "MountingHole_Pad" H 4600 3008 50  0000 L CNN
F 2 "" H 4500 3050 50  0001 C CNN
F 3 "~" H 4500 3050 50  0001 C CNN
	1    4500 3050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H?
U 1 1 6E5D69FE
P 4100 3050
F 0 "H?" H 4200 3099 50  0000 L CNN
F 1 "MountingHole_Pad" H 4200 3008 50  0000 L CNN
F 2 "" H 4100 3050 50  0001 C CNN
F 3 "~" H 4100 3050 50  0001 C CNN
	1    4100 3050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H?
U 1 1 6E5D6CC9
P 5250 2200
F 0 "H?" H 5350 2249 50  0000 L CNN
F 1 "MountingHole_Pad" H 5350 2158 50  0000 L CNN
F 2 "" H 5250 2200 50  0001 C CNN
F 3 "~" H 5250 2200 50  0001 C CNN
	1    5250 2200
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H?
U 1 1 6E5D70F8
P 5500 1950
F 0 "H?" H 5600 1999 50  0000 L CNN
F 1 "MountingHole_Pad" H 5600 1908 50  0000 L CNN
F 2 "" H 5500 1950 50  0001 C CNN
F 3 "~" H 5500 1950 50  0001 C CNN
	1    5500 1950
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H?
U 1 1 6E5D752F
P 5750 2200
F 0 "H?" H 5850 2249 50  0000 L CNN
F 1 "MountingHole_Pad" H 5850 2158 50  0000 L CNN
F 2 "" H 5750 2200 50  0001 C CNN
F 3 "~" H 5750 2200 50  0001 C CNN
	1    5750 2200
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H?
U 1 1 6E5D7875
P 5750 4050
F 0 "H?" H 5650 4007 50  0000 R CNN
F 1 "MountingHole_Pad" H 5650 4098 50  0000 R CNN
F 2 "" H 5750 4050 50  0001 C CNN
F 3 "~" H 5750 4050 50  0001 C CNN
	1    5750 4050
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H?
U 1 1 6E5D7DA7
P 5500 3900
F 0 "H?" H 5400 3857 50  0000 R CNN
F 1 "MountingHole_Pad" H 5400 3948 50  0000 R CNN
F 2 "" H 5500 3900 50  0001 C CNN
F 3 "~" H 5500 3900 50  0001 C CNN
	1    5500 3900
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H?
U 1 1 6E5D8098
P 5250 4100
F 0 "H?" H 5150 4057 50  0000 R CNN
F 1 "MountingHole_Pad" H 5150 4148 50  0000 R CNN
F 2 "" H 5250 4100 50  0001 C CNN
F 3 "~" H 5250 4100 50  0001 C CNN
	1    5250 4100
	-1   0    0    1   
$EndComp
Wire Wire Line
	5250 2300 5250 2450
Wire Wire Line
	5500 2050 5500 2450
Wire Wire Line
	5500 2450 5250 2450
Connection ~ 5250 2450
Wire Wire Line
	5250 2450 5250 2850
Wire Wire Line
	5500 2450 5750 2450
Connection ~ 5500 2450
Wire Wire Line
	5750 2450 5750 2300
Connection ~ 5750 2450
Wire Wire Line
	5750 3450 5750 3750
Wire Wire Line
	5750 3750 5500 3750
Connection ~ 5750 3750
Wire Wire Line
	5750 3750 5750 3950
Wire Wire Line
	5500 3800 5500 3750
Connection ~ 5500 3750
Wire Wire Line
	5500 3750 5250 3750
Wire Wire Line
	5250 4000 5250 3750
Connection ~ 5250 3750
Wire Wire Line
	4850 3150 4500 3150
Connection ~ 4850 3150
Connection ~ 4500 3150
Wire Wire Line
	4500 3150 4100 3150
Wire Wire Line
	6100 3150 6500 3150
Connection ~ 6100 3150
Connection ~ 6500 3150
Wire Wire Line
	6500 3150 6850 3150
$EndSCHEMATC
